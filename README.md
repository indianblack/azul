# Azul

Aplikacja komponentowa do gry wieloosobowej

# Plan działania

- założenie repozytorium  -> **DONE**
- analiza istniejących rozwiązań: https://buddyboardgames.com/azul, https://pl.boardgamearena.com/gamepanel?game=azul
- analiza kolorystyczna 
- analiza UX
- projekt graficzny gry w Figmie
- dobranie odpowiednich języków programowania i bibliotek umożliwiających zaaplikowanie ww elementów
- diagramy stanów gry

tbc.

# Analiza istniejących rozwiązań

TODO

# Analiza kolorystyczna 

Kolor jest bardzo ważny pod wieloma względami. Umiejętne wykorzystanie wiedzy z zakresu teorii kolorów może zdecydowanie ulepszyć projekt, gdyż kolor działa na odbiorców na poziomie podświadomości.

Ważnym rezultatem poprawnego doboru kolorów jest również zwiększenie czytelności aplikacji, szczególnie dla osób z wadami wzroku oraz żyjących w różnych kręgach kulturowych.

Dzieki zrozumieniu, które kolory najbardziej przemawiają do odbiorców, można znacząco zwiększyć ich satysfakcję z korzystania z aplikacji, co oczywiście wpłynie także na częstsze jej używanie.

Celem rozdziału będzie zapoznanie się z zasadami teorii koloru i stworzenie palety kolorystycznej, która następnie zostanie zaaplikowana w projekcie.

## Jak ludzie postrzegają kolor?

W świecie fizycznym kolor to po prostu światło, które ludzie odbierają poprzez zmysły. Widzenie kolorów jest subiektywnym wrażeniem powstającym w mózgu człowieka oraz niektórych zwierząt. (Wikipedia)

Można więc zadać pytanie - czym jest światło? Światło to fale elektromagnetyczne, tzw. promieniowanie widzialne odbierane przez siatkówkę oka. Promieniowanie widzialne, stanowiące część promieniowania elektromagnetycznego, obejmuje bardzo wąski zakres 380–750nm. Wyodrębnić w nim można przedziały o różnych długościach fal, które nasze oko odbiera jako wrażenia barwne, kolory. (Wikipedia) Ludzkie oko najlepiej odbiera barwy zawarte w środku promieniowania widzialnego, a najgorzej na końcach zakresu. (Światło widzialne)

(TODO:  wstawić ilustrację pokazująca zakres światła widzialnego)

Fale długie mają barwę czerwoną, a im stają się krótsze tym bardziej zbliżają się do koloru niebieskiego. (Colm Kelleher, Światło widzialne)

Za odbiór koloru w ludzkim oku odpowiedzialne są dwa rodzaje fotoreceptorów: czopki i pręciki. Pręciki są dużo bardziej czułę na światło niż czopki. Umożliwiają widzenie w słabym świetle, ale nie odbierają barw. 

Czopki umożliwiają widzenie kolorów przy dobrym oświetleniu. Występują w trzech rodzajach, z których każdy reaguje na inny zakres fali - kolor: czerwony, zielony lub niebieski. Dzięki ich współpracy możeby widzieć pełne spektrum barw. (James Eccleston)

(TODO: wstawić ilustrację pokazującą działanie ludzkiego oka)

## Teoria koloru

Badaniem powstawania wrażeń barwnych u człowieka (opisanym we wcześniejszym rozdziale) oraz czynnikami biorącymi udział w tym procesie zajmuje się interdyscyplinarny dział wiedzy zwany teorią koloru.

Zgodnie z teorią koloru barwy dzielimy na:
- podstawowe - czyli takie, których nie możemy uzyskać z połączenia innych kolorów: żółty, niebieski i czerwony
- pochodne - które powstają poprzez zmieszanie kolorów podstawowych

Teoria koloru wykorzystuje tez takie pojęcia jak:
- odcień - czyli czysty, nasycony kolor bez domieszek szarości, przyciemniania lub rozjaśniania
- nasycenie - czyli intensywnośc koloru.
- ton - który mówi jak jasna bądź ciemna jest barwa.

(Wstawić ilustrację pokazującą koło barw)

Koło kolorów możemy podzielić na ciepłe i zimne kolory. Zimne kolory to np. błękity, fiolety i  niektóre odcienie zieleni. Ciepłe to: czerwienie, pomarańcze i żółcie.

Duże znaczenie w projektowaniu ma równiez kontrast. Istnieją trzy rodzaje kontrastu:
- kontrast związany z odcieniem jest wtedy, kiedu użyjemy dwóch kolorów znajdujących się po przeciwnych stronach koła kolorów
- kontrast w nasyceniu - występujący przy mocno i słabo nazysonych barwach
- kontrast tonalny - pomiędzy jasnymi i ciemnymi barwami.

Do projektowania kolorów na ekrany monitorów używa się modelu przestrzeni barw RGB, natomiast do projektów przeznaczonych do druku używany jest model CMYK. (James Eccleston)

## Odbiór koloru a kultura, psychologia, kontekst

Jak widac z poprzedniego podrozdziału, za doborem i używaniem koloru stoi wiele teorii, ale jest też dużo subiektywności. Warto więc przyjżeć się jak kultura może wpłynąć na odbiór kolorów.

Trwałe skojarzenia dotyczące kolorów wykształciły się historycznie i zgodnie z książką Gavina Evansa prezentują się następująco:

- czerwony: miłość, ciepło, przyjemność, siła, agresywność, energia, odwaga, pasja, aktywność, krew
- zielony: spokój, zdrowie, naturalność, świeżość, 
- niebieski: smutek, spokój, pewność, bezpieczeństwo, stabilność, mądrość, nauka
- żółty: pozytywność, aktywność, frustracja, szczęście, tchórzostwo, optymizm
- czarny: śmierć, brzydota, noc, głębia, nienawiść, ciężar, strach, tajemniczość, elegancja, ciemność
- biały: spokój, lekkość, czystość, niewinnosć, życzliwość, zdrowie, świetlistość
- różowy: kobiecość, romantyczność, piękno
- fioletowy: bogactwo, luksus, ambicja, ekstrawagancja
- brązowy: ciepło, bezpieczeństwo, niezawodność, smutek, samotność
- pomarańczowy: optymizm, ciepło, pasja, entuzjazm, kreatywność, zabawa, ostrzeżenie

Kolory działają na nas psychologicznie, ale ich rzeczywisty efekt jest dużo bardziej skomplikowany i subtelny niż to co opisuje wiele poradników dotyczących projektowania. 

Przede wszystkim warto wzrócić uwagę na to, że niektóre kultury mają więcej kolorów w swoim słownictwie niż inne. Przykładem mogą być Starożytni Grecy, którzy nie mieli słowa na określenie koloru niebieskiego. (James Eccleston)

Różnice językowe wpływają również na to jak postrzegamy kolor. Naukowcy z Goldsmiths University w Londynie badając kulture afrykańskiego plemienia Himba zauważyli, że podobnie jak starożytni Grecy nie mają oni okreslenia na kolor niebieski. Kiedy w ramach badania przedstawiono im okrąg złożony zielonych kwadratów i jednego niebieskiego, a następnie poproszono o wybór jednego kwadratu, który był inny od pozostałych - mieli oni trudności ze znalezieniem owego jednego, niebieskiego kwadratu. Mieli natomiast dużo więcej określeń na kolor zielony i dużo łatwieuj było im odróżniać różne odcienie tego koloru. (Nicola Pitchford, str. 161)

Jakie ma to znaczenia dla projektowania aplikacji?
Jeżeli chcielibyśmy np. zaprojektować mobilną aplikację bankową dla plemienia Himba mając wiedze wynikającą z opisanych powyżej badań wiedzielibyśmy, że w momencie kiedy chcielibyśmy wykorzystać kontrasty aby wyodrębnić kluczowe dla aplikacji przyciski czy akcje, raczej słabym pomysłem byłoby wykorzysystanie dużej ilości odcieni zieleni i niebieskiego. 

Oczywiście jest to uproszczenie, gdyż ludzie sa dużo bardziej skomplikowani, ale takie niuanse powinny być brane pod uwagę kiedy projektuje się dla ludzi z innych kręgów kulturowych. Zdecydowanie trzeba pamiętac o zebraniu informacji, zrobieniu badań, a nawet porozmawiania z przedstawicielami danej kultury, aby uzyskac opinie z pierwszej ręki.

To co jest podobne w obrębie różnych kultur to tworzenie nazw kolorów w określonej kolejności. Najczęściej na początku powstają słowa określające czerń i biel (ciemnośc i światło). Następnie czerwony. Zaraz po tym zielony lub żółty. Potem niebieski. A na koniec: szary, fioletowy, różowy, pomarańczowy i brązowy. Widac więc, że pomimo wielu różnic, łączy nas wspólne rozumienie tego jak interpretujemy kolory i otaczający nas świat. (The surprising pattern behind color names around the world)

Kolory moga także nabierać różnych znaczeń w zalezności od kultury. W Chinach kolor czerwony symbolizuje bogactwo i szczeście. Kolor w kulturze może także zmienić swoje znaczenie z czasem. Na wyspach Brytyjskich tkaniny w kolorze zielonym używane były do szycia sukien ślubnych, gdyż kolor ten symbolizował płodność. Kiedy na wyspy dotarło chrześcijaństwo, kolor sukni ślubnych zmienił się z zielonego na biały. (TODO: znaleźć lepszy przykład)

Niektóre preferencje kolorystyczne tkwią głęboko w naszej podświadomości. Nie jesteśmy pewni dlaczego coś nie wygląda dobrze, ale czujemy, że dana kombinacja kolorystyczna jest niewłaściwa. Dla wielu ludzi na całym świecie podobać się moga podobne kolory, a związane jest to z ewolucją. Ewolucja zakorzeniła w nas chociażby nieufność w stosunku do koloru zielonego i szarego w jedzeniu, ponieważ może to oznaczać, że dana potrawa jest zepsuta. Niektóre owoce wręcz sygnalizują, że są dojrzałe i gotowe do spożycia poprzez zmiane koloru z zielonego na czerwony. 

(TODO: wstawić ilustrację pokazującą ulubione kolory)

Większość ludzi określa niebieski jako swój ulubiony kolor. W grupie mężdzycn drugim ulubionym komorem jest zielony, a u kobiet - fioletowy. Na ogół mężczyźni preferują bardziej nasycone barwy, kobiety zaś delikatniejsze, pastelowe odcienie. 

Projektując paletę kolorów dla danej aplikacji należy wziąźć pod uwagę upodobania konkretnej grupy docelowej. Pomocna może być analiza podobnych aplikacji znajdujących się już na rynku. Często może to pomóc w decyzji, czy podążyć za panującym i sprawdzonym trendem w danej niszy lub złamanie tej zasady aby się wyróżnić i zrobić coś innego. 

Podążanie za uogólnieniami i trendami kolorystycznymi w projektowaniu może spowodować, że dana paleta kolorystyczna stanie się nieco zbyt oczywista a przez to nudna. 

Ostatnnia rzeczą, na którą nalezy zwrócić uwagę jest kontekst. Znaczenie koloru czerwonego zmieni się w zależności od tego, czy uzyty został w samochodzie, skesownej sukience, kopercie czy na przycisku 'Stop'.

## Harmonia kolorów i jak ją stworzyć?

Projektując palete kolorów dla aplikacji ważne jest, aby była ona harmoniczna - czyli żeby kolory współgrały ze soba i były miłe dla oka. 

Aby to osiągnąć można uzyć specjalnie do tego przygotowanych programów, które bazując na teorii kolorów i kole barw pomagają w osiągnięcciu opisanego wyżej efektu.

W projekcie do tego celu uzywane będa dwa programy dostępne online: 

[Eva Design System](https://colors.eva.design/) jest programem umożliwiająca łatwe komponowanie palety koloów dla aplikacji użytkowych. Pomaga w szybkim skomponowaniu palety kolorów dla elementów funkcyjnych aplikacji: koloru podstawowego, stanowiącego rdzeń, oraz harmonizujących z nim kolorów oznaczająych akcje: powodzenia (sukces), informacyjne, ostrzegające i zagrożenia. Jesto to ważne aby przekazywać klarowne komunikaty odbiorcom aplikacji ułatwiające jej wykorzystywanie i zrozumienie. 

(TODO: wstawić screen Eva Design System)

[Koło kolorów Adobe](https://color.adobe.com/pl/create/color-wheel) poprzez zastosowanie różnych reguł harmonii kolorów pozwala na dopasowanie komplementujących się barw. W przypadku planowanej aplikacji będzie do miało duże znaczenie, gdyż oprócz kolorów funkcyjnych będzie musiało pojawić się wiele dodatkowych barw wykorzystywanych w projekcie kafli i tła.

(TODO: wstawić screen Koła Kolorów Adobe)

Dobrze jest użyć harmonii kolorów jako podstawy, a następnie dostosowywanie jej aby dopasować barwy do charakteru swojej pracy i efektu jaki chce się osiągnąć.

Warto też rozwazyć parowanie kolorów tworzących wspólnie harmonię kolorystyczna z kolorami neutralnymi takimi jak czarny czy biały. 

Jezeli chcemy wykorzystac kolory dopełniające lub po prostu użyć wielu kolorów w naszej aplikacji to aby nie było pomiędzy nimi zbyt dużego kontrastu i były przyjemniejsze dla oka, można rozważyć zmniejszenie kontrastu pomiędzy nimi, poprzez ich rozjaśnienie, przyciemnienie bądź zmniejszenie nasycenia barwy. 

Im więcej w palecie kolorów pojawi się zestawień dopełniających, tym bardziej dynamiczna i energetyzująca wyda się dana kombinacja. Nalezy jednak pamiętać aby nie przesadzić z taką harmonią barwną, gdyż może się to stać nieco przytłaczające. Takie zestawienia można neutralizować barwami neutralnymi wypełniającymi przestrzeń pomiędzy elementami o nasyconych kolorach. Warto też pomyślec o zostawieniu tylko jednej barwy intensywnie nasyconej jako barwy przewodniej. 

Aby stworzyć poprawny balans pomiędzy kolorami mozna zastosowac tzw. zasadę 60-30-10. Zasada ta pomaga określić ile którego koloru powinniśmy użyć. Polega ona na wyznaczeniu 60% przestrzeni kolorystycznej projektu na jeden kolor lub grupę kolorów,30% na kolejną i 10% na ostatnią. Najczęściej 10% przestrzeni kolorystycznej zajmują najbardziej nasycone, zywe kolory. 

Sprawdzoną praktyką jest również dodanie białego lub innej neutralnej barwy tak aby zajmowałam 60% przestrzeni kolorystycznej. Dzięki temu użytkownik aplikacji nie czuje się bombardowany cięzkimi kolorami.
(James Eccleston)

## Stosowanie palety kolorów w designie

Należy pamiętać, że dobór kolorów jest prosesem bardzo subiektywnym. Często tez łatwiej go przeprowadzić po nabyciu pewnej ilości doświadczenia. Nalezy zawsze kierowac się zdrowym rozsądkiem. Nawet jeżeli paleta kolorów została już zdefiniowana, ale po zastosowaniu określonych barw w designie wciąż coś nie gra, nalezy cofnąc się i zadac sobie pytania: czy dana paleta kolorów się tu sprawdza? Czy połączenie kolorów jest poprawne? I wrazie niezadawalających odpowiedzi powtórzyć proces bądź dokonac niezbędnych zmian.

Kilka rzeczy, o których należy pamiętać podczas stosowania kolorów to:

- Czy kolory są czytelne?
- Czy jest pomiędzy nimi wystarczający kontrast?
- Czy jest za dużo kolorów? Może trzeba ogranmiczyć wybraną paletę?
- Czy nalezy dodać biały, czarny bądź szary?

Odcienie szarości i czarny często się przydają, szczególnie przy projektowaniu stron internetowych. 

Czerń jest przydatna szczególnie do liternictwa. Nie zaleca się jednak stosowanie stuprocentowej czerni. Lepiej zastosować kolor zbliżony, ale nie jednolicie czarny, gdyż taka czerń ma bardzo duzy kontrast i może być zbyt surowa w odbiorze.

Szarości mogą występowac w różnych odcieniach. Często bardzo jasne szarości stosuje się jako tło dla tekstu lub sekcji strony internetowej. Przy określaniu szarości warto nie wybierać naprawde neutralnych odcieni, lecz przesunąć barwę w kierunku zimnej szarości (zmieszanej z odrobiną niebieskiego) lub ciepłej szarości (zmieszanej z odrobiną czerwieni) gdyż daje to bardziej interesujące efekty.

Warto na początek poeksperymentowac z kolorami na fragmencie designu, gdyż pomoże to w wyłapaniu ewentualnych błędów i niedociągnięć. Gdy poczujemy się już komfortowo z tym, jak kolor wygląda na wybranym fragmencie, możemy zastosowac w całym projekcie. 

W Figmie, programie który użyty zostanie do projektowania designu aplikacji, istnieją wtyczki umożliwiające sprawdzanie kontrastu pomiędzy kolorami. W trakcie eksperymentowania z kolorami warto co jakiś czas upewniac się czy zachowany jest poprawny kontrast kolorystyczny, aby zapewnić czytelność aplikacji (szczególnie dla osób z wadami wzroku).

Przy wdrażaniu kolorów warto zadać sobie następujące pytania:
- Jakie emocje ma wywoływać w użytkowniku dana kombinacja kolorystyczna?
- Jaka jest nasza grupa docelowa? Jakie osoby będa korzystały z aplikacji?
- Do jakiego kręgu kulturowego zaliczają się użytkowicy aplikacji?
- Jak palety kolorystyczne zostały dobrane w innyych tego typu rozwiązaniach? Czy mpżemy się nimi zainspirować?
- Czy wszystkie elementy aplikacji są czytelne?
- Czy odpowiednio przypisujemy kolory funkcyjne? Czy sa one spójne w całej aplikacji?

Aby zapewnić zaangażowanie użytkowników nalezy zwrócić szczególną uwagę na barwę elementów "call to action". Najprościej mówiąc, są to te elementy, które mają na celu zwrócić uwagę użytkownika aby wykonał jakąś czynność. Zalicza się do nich chociażby przyciski, odnośniki, ale moga mieć też postać samodzielnie pojawiającego się tekstu z zaszytym linkiem. 

Ciekawe badanie na temat koloru przycisków zostało przeprowadzone przez firmę HubSpot. Przebadali oni grupę 2.000 użytkowników pokazując im dwa różne kolory przycisków wzywających do działania: zielony i czerwony. Wszystie pozostałe elementy strony pozostały niezmienione. (HubSpot)

(TODO: wstawić screenshot przycisków ze strony HubSpot)

Wynik? 21% więcej osób kliknęło czerwony przycisk niż zielony. Jest to potężna różnica, która oznacza poprawienie wydajności strony i zaangażowania odwiedzających o 1/4 tylko poprzez odpowiednie dobranie kolorów elementów wzywających do działania. (HubSpot)

Ogólnie sprawdzone kolory takich elementów to: zielony, czerwony, pomarańczowy lub niebieski. Niektóre kolory, których raczej powinno się unikać przy elementach zwywających do działania to: szarny, czarny i brązowy. Ale zalezy to oczywiście od tego jaka paleta kolorystyczna została zastosowana w danym designie i jakie są wymagania wynikające z grupy docelowej, do której kieruje się dany projekt. Najważniejsze aby wyróżniały się one na tle reszty designu.

Najważniejsze zasady, których nalezy się trzymać projektując elementy "call to action" to:
1. Powinny być spójne z przyjętą paletą kolorystyczną.
2. Powinny mieć barwę kontrastującą/wyróżniająca się na tle pozostałych elementów.
3. Powinny być czytelne.

Warto też pamiętać o paru wskazówkach, które pomoga naprawić ewentualne błędy popełnione podczas tworzenia palety kolorystycznej:

1. Staraj się zawsze nie stosować zbyt dużej ilości kolorów.
2. Ograniczaj ilośc mocno nasyconych barw.
3. Pilnuj aby kontrast kolorów był zachowany i wszystkie elementy były czytelne.
4. Zachowaj harmonię barw i spójność kolorystyczną.
5. Kolory elementów funkcyjnych (np. klikalnych przycisków, linków etc.) powinny być jednolite w całym projekcie.


(James Eccleston)

# Stosowanie palety kolorów do ilustracji

Aby przyspieszyć i ułatwić dostosowywanie ilustracji to wybranej palety kolorów można stworzyć parę wariacji tonalnych (jasnych i ciemniejszych odcieni) wybranych kolorów podstawowych. 

Następnie, aby zachowac balans kolorów, zastosować zasadę 60-30-10, mając w tyle głowy w jaki sposób zasadę tę zaaplikowaliśmy w całości aplikacji. 

Dostosowywanie ilustracji również wymaga eksperymentowania i nie nalezy się tego obawiać. Jest jednak kilka zasad o których warto pamiętać:
1. Staraj się ograniczać ilość kolorów stosowanych w ilustracjach - im więcej kolorów jest wykorzystywane tym trudniej je ze sobą zharmonizować.
2. Zredukuj liczbę intensywanie nasyconych barw. Nie trzeba robić tego zabiegu na wszystkich kolorach. Najwazniejsze żeby pamiętać jakie emocje ma przekazywac dana ilustracja.
(James Eccleston)
3. Kontroluj czy zachowany jest odpowiedni kontrast i czy elementy ilustracji sa czytelne.
4. Oceniaj, czy całość ilustracji harmonizuje ze sobą i czy nie trzeba zmienić wybranych kolorów. 

# Literatura:

## Analiza kolorystyczna 

James Eccleston, platforma [Domestika](https://www.domestika.org/), kurs "Teoria kolorów w projektach online"

Patti Mollica, ["Color Theory: An essential guide to color - from basic principles to practical applications"](https://books.google.pl/books/about/Color_Theory.html?id=9VK3OIpiYagC&redir_esc=y), Walter Foster Publishing, 2013 

Johannes Itten, ["The art of color: the subjective experience and objective rationale of color"](https://books.google.pl/books/about/The_Art_of_Color.html?id=D-skaDZAumIC&redir_esc=y), John Wiley, 1974

Betty Edwards, ["Color: A Course in Mastering the Art of Mixing Colors"](https://books.google.pl/books/about/Color.html?id=LDFjQgAACAAJ&redir_esc=y), Tarcher, 2004

John Cantwell, Eddie Opara, ["Color works: best practices for graphic designers: an essential guide to understanding and applying color design principles"](https://www.goodreads.com/book/show/15850812-color-works), Rockport Publishers, 2014

[Teoria koloru](https://pl.wikipedia.org/wiki/Teoria_koloru), Wikipedia

[Widmo światła widzialnego, długość fali światła](https://lenalighting.pl/o-nas/baza-wiedzy/941-widmo-promieniowania-widzialnego)

[Światło widzialne](https://www.medianauka.pl/swiatlo-widzialne)

Nicola Pitchford, Carole P. Biggam, ["Progress in Colour Studies:Volume II. Psychological Aspects](https://books.google.pl/books/about/Progress_in_Colour_Studies.html?id=QYU6AAAAQBAJ&redir_esc=y), John Benjamins Publishing Company, 2006

["The surprising pattern behind color names around the world"](https://www.youtube.com/watch?v=gMqZR3pqMjg), Vox

Colm Kelleher, ["Czym jest kolor?"](https://www.youtube.com/watch?v=UZ5UGnU7oOI&list=PLF40RYwZ15cdeOa03mDlC_rxRfopNJaiP&index=9), TED

Gavin Evans, ["The Story of Colour: An Exploration of the Hidden Messages of the Spectrum"](https://books.google.pl/books/about/The_Story_of_Colour.html?id=caUxDwAAQBAJ&redir_esc=y), Michael O’Mara Books, 2017

HubSpot, ["The Button Color A/B Test: Red Beats Green"](https://blog.hubspot.com/blog/tabid/6307/bid/20566/the-button-color-a-b-test-red-beats-green.aspx)
